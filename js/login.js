
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, signOut, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-auth.js";

const firebaseConfig = {
  apiKey: "AIzaSyCPhqHoNMQhTDFzi5kUJQTrxT6j2le8Ib8",
  authDomain: "administradorweb-93c5d.firebaseapp.com",
  databaseURL: "https://administradorweb-93c5d-default-rtdb.firebaseio.com",
  projectId: "administradorweb-93c5d",
  storageBucket: "administradorweb-93c5d.appspot.com",
  messagingSenderId: "776263295548",
  appId: "1:776263295548:web:a8e473c35a130630875939"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

function login(){
    event.preventDefault();
    let email = document.getElementById('email').value;
    let contra = document.getElementById('contraseña').value;

    if(email == "" || contra == ""){
        alert('Complete los campos');
        return;
    }
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, contra).then((userCredential) => {
    alert('Bienvenido ' + email);
    sessionStorage.setItem('isAuth',"true");
    window.location.href = 'administrador.html';
    
    })
    .catch((error) => {
        alert('Usuario y o contraseña incorrectos')
    });

}



var btnCerrarSesion = document.getElementById('btnCerrarSesion');

if(btnCerrarSesion){
    btnCerrarSesion.addEventListener('click',  (e)=>{
        signOut(auth).then(() => {
        alert("SESIÓN CERRADA")
        window.location.href="/index.html";

        }).catch((error) => {
  
        });
    });
}

onAuthStateChanged(auth, async user => {
    console.log(window.location.pathname);
    if (user) {

    } else {
        if (window.location.pathname.includes("administrador")) {
            window.location.href = "/index.html";
        }
    }
});

var botonLogin = document.getElementById('btnIniciarSesion');

if(botonLogin){
    botonLogin.addEventListener('click', login);
}

import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-app.js";
import { getDatabase, onValue, ref, set, child, get, update, remove} from "https://www.gstatic.com/firebasejs/9.17.2/firebase-database.js";
import { getStorage,ref as refStorage, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyCPhqHoNMQhTDFzi5kUJQTrxT6j2le8Ib8",
    authDomain: "administradorweb-93c5d.firebaseapp.com",
    databaseURL: "https://administradorweb-93c5d-default-rtdb.firebaseio.com",
    projectId: "administradorweb-93c5d",
    storageBucket: "administradorweb-93c5d.appspot.com",
    messagingSenderId: "776263295548",
    appId: "1:776263295548:web:a8e473c35a130630875939"
  };

  // Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();

// Generar Productos
let productos = document.getElementById('contenido-productos');
window.addEventListener('DOMContentLoaded',mostrarProductos);
let teclados = document.getElementById('teclados');
let mouse = document.getElementById('mouse');
let audifonos = document.getElementById('audifonos');
let monitores = document.getElementById('monitores');
let microfonos = document.getElementById('microfonos');
let impresoras = document.getElementById('impresoras');
let parlantes = document.getElementById('parlantes');
let camaraWeb = document.getElementById('camaraWeb');
let complementos = document.getElementById('complementos');
let todos = document.getElementById('todos');

function mostrarProductos(){
    const dbRef = ref(db, "productos");


    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
        
        if(childData.estatus=="1"){

            productos.innerHTML +=`
                <div class='producto'>
                <img class='img-item' src='${childData.urlImagen}'>
                <p class='nombre'>${childData.nombre}</p>
                <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                <p class='precio'>\$${childData.precio}</p>
                <button class='boton-comprar' data-bs-toggle="modal" data-bs-target="#exampleModalToggle">Comprar</button>
                </div>
            `;
        }
        
        });
    },
    {
        onlyOnce: true,
    }
    );
}




function mostrarTeclados(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Teclados";
    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==1 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
} 


function mostrarMouse(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Mouse";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
            snapshot.forEach((childSnapshot) => {
                const childData = childSnapshot.val();
                
                
                if(childData.categoria==2 && childData.estatus=="1"){
                    productos.innerHTML +=`
                        <div class='producto'>
                        <img class='img-item' src='${childData.urlImagen}'>
                        <p class='nombre'>${childData.nombre}</p>
                        <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                        <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                        <p class='precio'>\$${childData.precio}</p>
                        <button class='boton-comprar'>Comprar</button>
                        </div>
                    `;
                    
                }
                
                
            });
    },
    
    {
        onlyOnce: true,
    }
    );
    
} 

function mostrarAudifonos(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Audifonos";
    
    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            

                if(childData.categoria==3 && childData.estatus=="1"){
                    productos.innerHTML +=`
                        <div class='producto'>
                        <img class='img-item' src='${childData.urlImagen}'>
                        <p class='nombre'>${childData.nombre}</p>
                        <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                        <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                        <p class='precio'>\$${childData.precio}</p>
                        <button class='boton-comprar'>Comprar</button>
                        </div>
                    `;
                    
                }
                
                
            });
        },
        
        {
            onlyOnce: true,
        }
        );
        
    } 

function mostrarMonitores(){
    const dbRef = ref(db, "productos");

    document.getElementById('tituloProductos').innerHTML = "Monitores";
    
    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==4 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
}
);

} 

function mostrarMicrofonos(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Microfonos";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==5 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
    
}

function mostrarImpresoras(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Impresoras";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==6 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
    
}

function mostrarParlantes(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Parlantes";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==7 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
    
} 

function mostrarCamaras(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Camaras Web";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==8 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
    
}

function mostrarComplementos(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Complementos";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==9 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
    
} 

function mostrarTodos(){
    const dbRef = ref(db, "productos");

    document.getElementById('tituloProductos').innerHTML = "Nuestros Productos";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
        
        if(childData.estatus=="1"){
            productos.innerHTML +=`
                <div class='producto'>
                <img class='img-item' src='${childData.urlImagen}'>
                <p class='nombre'>${childData.nombre}</p>
                <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                <p class='precio'>\$${childData.precio}</p>
                <button class='boton-comprar'>Comprar</button>
                </div>
            `;
        }
        
        });
    },
    {
        onlyOnce: true,
    }
    );
}

teclados.addEventListener('click', mostrarTeclados);
mouse.addEventListener('click', mostrarMouse);
audifonos.addEventListener('click', mostrarAudifonos);
monitores.addEventListener('click', mostrarMonitores);
microfonos.addEventListener('click', mostrarMicrofonos);
impresoras.addEventListener('click', mostrarImpresoras);
parlantes.addEventListener('click', mostrarParlantes);
camaraWeb.addEventListener('click', mostrarCamaras);
complementos.addEventListener('click', mostrarComplementos);
todos.addEventListener('click', mostrarTodos);
